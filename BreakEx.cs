using System;
 
class Code
{
	static void Main()
	{
		//считываем два числа
		int a,b;
		a = Int32.Parse(Console.ReadLine());
		b = Int32.Parse(Console.ReadLine());
		
		//отрабатываем случай, когда a > b
		if (a > b) 
		{
			Console.WriteLine("Будем менять местами  a и b?");		
			string s = Console.ReadLine();		
			if (s == "Нет") return;
			//можно опустить else
			//иначе 
			int c = a;
			a = b;
			b = c;
		}
		
		//в цикле проходим по всем целым числам от a до b
		
		//количество кратных пяти чисел
		int k5 = 0;
		
		while(a <= b)
		{
			//проверяю кратность трем
			if((a % 3) == 0)
				Console.WriteLine(a);
			if((a % 5) == 0)
				k5++;
			if(k5 > 3) 
			{
				Console.WriteLine("Кратных пяти чисел больше 3, выполнение цикла остановлено");
				break;
			}
			a++;
		}
		Console.WriteLine(k5);	
		Console.ReadLine();		

	}	
}