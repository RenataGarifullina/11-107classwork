﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    public abstract class Arch
    {
        public abstract void Arhive();
        public abstract void DisArhive();
    }

    public class Rar : Arch
    {
        public override void Arhive()
        {
            Console.WriteLine("Заархивирован .rar");
        }
        public override void DisArhive()
        {
            Console.WriteLine("Разархивирован .rar");
        }
    }

    public class Zip : Arch
    {
        public override void Arhive()
        {
            Console.WriteLine("Заархивирован .zip");
        }
        public override void DisArhive()
        {
            Console.WriteLine("Разархивирован .zip");
        }
    }

    public class Zipx : Zip
    {
        public override void Arhive()
        {
            Console.WriteLine("Заархивирован .zipx");
        }
        public override void DisArhive()
        {
            Console.WriteLine("Разархивирован .zip");
        }
    }
}