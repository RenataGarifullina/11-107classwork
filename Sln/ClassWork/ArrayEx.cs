﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    public class ArrayEx
    {
        public void Run() 
        {
            //массив, в котором хранятся входные данные
            int[] intArr;
            #region Чтение входных данных
            //Считали числа в строку через пробел
            string s = Console.ReadLine();
            //Проверили на пустоту строку
            if (String.IsNullOrWhiteSpace(s))
            {
                Console.WriteLine("Введена пустая строка, действие прекращено");
                return;
            }
            //Разбиваем строку на подстроки по пробелу
            string[] sArr = s.Split(' ');
            intArr = new int[sArr.Length];
            for(int i = 0; i < sArr.Length; i++) 
            {
                intArr[i] = Int32.Parse(sArr[i]);
            }
            #endregion

            int maxValue;
            #region Поиск максимального значение
            maxValue = intArr[0];
            for (int i = 1; i < intArr.Length; i++) 
            {
                if (maxValue < intArr[i])
                    maxValue = intArr[i];
            }
            #endregion

            #region Вывод результата
            Console.WriteLine("Максимальное значение равно " + maxValue);
            Console.WriteLine($"Максимальное значение равно {maxValue}");
            #endregion
        }
    }
}
