﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Большое число
    /// </summary>
    public class BigNumber
    {
        /// <summary>
        /// Внутреннее хранение большого числа
        /// </summary>
        private int[] num;

        public int[] Number
        {
            get { return num; }
            set 
            {
                if (value == null)
                {
                    Console.WriteLine("Значение большого числа равно null, оно не сохранено");
                    return;
                }
                num = value;
            }
        }

        /// <summary>
        /// Конструктор с параметром
        /// </summary>
        /// <param name="number">Само число</param>
        public BigNumber(BigNumber number) 
        {
            if (number == null)
                throw new NullReferenceException("Значение большого числа не может равняться null");

            num = number.Number;
        }

        /// <summary>
        /// К num прибавляется число term и записывается в num
        /// </summary>
        /// <param name="term"></param>
        public void Add(BigNumber term)
        {
            //todo доделать самим
        }

        /// <summary>
        /// Вычитается из num значение subtrahend
        /// </summary>
        /// <param name=""></param>
        public void Difference(BigNumber subtrahend) 
        { 
        }

        /// <summary>
        /// Умножение на число
        /// </summary>
        /// <param name="number"></param>
        public void MultiplyBuNum(BigNumber number) 
        { 
        }
    }
}
