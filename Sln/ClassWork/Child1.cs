﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Дочерний класс для класса Parent
    /// </summary>
    public class Child1: Parent
    {
        /// <summary>
        /// Публичное поле дочернего класса
        /// </summary>
        public string ChildPublicField;

        /// <summary>
        /// Конструктор дочернего класса
        /// </summary>
        public Child1() 
        {
            PublicField = 13;
            ProtectedField = 14;            
            Console.WriteLine("Сработал дочерний конструктор без параметров");
        }

        public Child1(int publicFieldValue, int protFieldValue, string childFieldValue) 
            : base(publicFieldValue, protFieldValue)
        {
            //PublicField = publicFieldValue;
            //ProtectedField = protFieldValue;
            ChildPublicField = childFieldValue;
            Console.WriteLine("Сработал дочерний конструктор c параметрами");
        }

        public override void VirtualMethod()
        {
            Console.WriteLine("Отработал переопределенный метод дочернего класса");
        }

        public new void UsualMethod() 
        {
            Console.WriteLine("Перекрывание родительского обычного метода в дочернем");
        }
    }
}
