﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
   public  class Company
    {
        public void Delivery(DeliveryObject d2)
        {
            DeliveryMan dm;
            var c = new Car();
            var b = new Bicycle();
            var w = new Walk();



            if (d2.Weight <= w.Capacity) dm = w;
            else if (d2.Weight <= b.Capacity) dm = b;
            else dm = c;
            dm.Delivery(d2);

        }
    }
}

