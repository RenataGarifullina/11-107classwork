﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
     public abstract class DeliveryMan
    {
        public abstract void Delivery(DeliveryObject d);
        public abstract int Capacity { get; set; }
        
    }
    public class Car : DeliveryMan
    {
        public override void Delivery(DeliveryObject d)
        {
            Console.WriteLine("Машина доставляет " + d.Name);
        }
        public override int Capacity { get; set; } = 100;
    }
    public class Bicycle : DeliveryMan
    {
        public override void Delivery(DeliveryObject d)
        {
            Console.WriteLine("Велосипед доставляет " + d.Name);
        }
        public override int Capacity { get; set; } = 10;

    }
    public class Walk : DeliveryMan
    {
        public override void Delivery(DeliveryObject d)
        {
            Console.WriteLine("Пешим образом доставляется " + d.Name);
        }
        public override int Capacity { get; set; } = 5;

    }
}
