﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    public abstract class DeliveryObject
    {
        public string Name { get; set; }
        public int Weight { get; set; }
        public int Time { get; set; }

    }
    public class Food: DeliveryObject
    {
        public int Temperature { get; set; }
    }
    public class Thing : DeliveryObject
    {
        public string Broken { get; set; }
    }
}

