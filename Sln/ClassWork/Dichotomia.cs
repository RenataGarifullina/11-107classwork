﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    public class Dichotomia
    {
        public void Run()
        {
            int[] arr = new int[] { 11, 99};
            int result = Peak(arr, 0, arr.Length);
        }

        public int Peak(int[] array, int l, int r)
        {

            if (array.Length == 1)
                return array[0];
            if (r - l == 1)
            {
                return array[l] > array[r] ? array[l] : array[r];
            }
            int middle = (r + l) / 2 + (r + l) % 2;

            if ((middle == array.Length - 1 ||  array[middle + 1] < array[middle]) 
                && (middle == 0 || array[middle - 1] < array[middle]))
            {
                return array[middle];
            }
            else if (middle == 0 || array[middle - 1] < array[middle])
                return Peak(array, middle, r);
            else
                return Peak(array, l, middle);
        }
    }
}
