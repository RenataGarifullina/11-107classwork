﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    class Dog:HomePet
    {
        public string VeterinarianVisit{get; set; }
        public bool PassportYesOrNot { get; set; }
        public override void Feed()
        {
            Console.WriteLine("Кормление стандартными кормами");
        }
        public void Taking_Care()
        {
            Console.WriteLine("Ухаживать");
        }
    }
}
