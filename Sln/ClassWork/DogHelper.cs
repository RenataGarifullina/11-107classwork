﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    class DogHelper:Dog
    {
        public bool AttachedToTheHost { get; set; }
        public int AgeWork { get; set; }
    }
}
