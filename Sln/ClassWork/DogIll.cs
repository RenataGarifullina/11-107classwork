﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    class DogIll:Dog
    {
        public string Diagnosis { get; set; }
        public string Health { get; set; }
        public new void Taking_Care()
        {
            Console.WriteLine("Ухаживаем за больной собакой");
        }
        public override void Feed()
        {
            Console.WriteLine("Покормили больную собаку");
        }
    }
}
