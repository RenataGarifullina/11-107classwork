﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    public static class StringExtension
    {
        public static string OneByOneJoin(this string s1, string s2) 
        {
            //todo реализовать самим
            return string.Empty;
        }

        public static void Rainbow(this String input)
        {

            ConsoleColor[] colors = new ConsoleColor[]
            { ConsoleColor.Red,
              ConsoleColor.Yellow,
              ConsoleColor.Green,
              ConsoleColor.Blue,
              ConsoleColor.DarkBlue,
              ConsoleColor.Magenta
            };
            int color_index = 0; // from 0 to 5

            string[] str = input.Split(' ');

            foreach (var el in str)
            {
                Console.ForegroundColor = colors[color_index];
                Console.Write($"{el} ");
                color_index++;
                color_index %= 6;
            }
        }
    }
}
