﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork.IPlayer
{
    public class FootballPlayer : IPlayer
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public FootballPlayer(string name, int age) 
        {
            Name = name;
            Age = age;
        }
        public void Play()
        {
            Console.WriteLine();
        }
    }
}
