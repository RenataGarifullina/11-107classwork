﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork.IPlayer
{
    /// <summary>
    /// Игрок
    /// </summary>
    public interface IPlayer
    {
        /// <summary>
        /// Играть
        /// </summary>
        void Play();
    }
}
