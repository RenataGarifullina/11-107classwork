﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork.IPlayer
{
    /// <summary>
    /// Музыкант
    /// </summary>
    public class Musician : IPlayer
    {
        public string Name { get; set; }
        public Instruments Instrument { get; set; }
        public Musician(string name, Instruments instrument)
        {
            Name = name;
            Instrument = instrument;
        }
        public void Play()
        {
            Console.WriteLine($"Музыкант" + Name + $" играет на {Instrument}");
        }
    }

    /// <summary>
    /// Инструменты
    /// </summary>
    public enum Instruments
    { 
        piano,
        violin,
        cello
    }
}
