﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    class CinemaStudent : Student, IActor
    {
        public CinemaStudent(String s) : base(s) { }
        public void Play()
        {
            Console.WriteLine("Студент актёр кино играет роль.");
        }

        public override void Study()
        {
            StudyTheory();
            Play();
        }

        public override void StudyTheory()
        {
            Console.WriteLine("Студент актёр кино изучает теорию.");
        }
    }
}
