﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Актер
    /// </summary>
    interface IActor
    {
        /// <summary>
        /// Играть
        /// </summary>
        void Play();
    }
}
