﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Художник
    /// </summary>
    public interface IPainter
    {
        /// <summary>
        /// Сделать набросок
        /// </summary>
        void MakeScetch();
        /// <summary>
        /// Нанести краску
        /// </summary>
        void ApplyPaint();
    }
}
