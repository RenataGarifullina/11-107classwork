﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Певец
    /// </summary>
    public interface ISinger
    {
        /// <summary>
        /// Петь
        /// </summary>
        void Sing();
    }
}
