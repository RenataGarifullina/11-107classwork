﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Изучение теории
    /// </summary>
    interface ITheory
    {
        /// <summary>
        /// Изучать теорию
        /// </summary>
        void StudyTheory();
    }
}
