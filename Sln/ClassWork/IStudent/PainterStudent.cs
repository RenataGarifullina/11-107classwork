﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    class PainterStudent : Student, IPainter
    {
        public PainterStudent(string s) : base(s) { }
        void IPainter.ApplyPaint()
        {
            Console.WriteLine("Студент художник наносит краски.");
        }

        void IPainter.MakeScetch()
        {
            Console.WriteLine("Студент художник делает набросок.");
        }

        public override void Study()
        {
            StudyTheory();
            ((IPainter)this).MakeScetch();
            ((IPainter)this).ApplyPaint();
        }

        public override void StudyTheory()
        {
            Console.WriteLine("Студент-художник изучает теорию.");
        }
    }
}
