﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    class SingerStudent : Student, ISinger
    {
        public SingerStudent(string s): base(s)
        {
        }
        public void Sing()
        {
            Console.WriteLine("Студент певец поёт.");
        }

        public override void Study()
        {
            StudyTheory();
            Sing();
        }

        public override void StudyTheory()
        {
            Console.WriteLine("Студент певец изучает теорию.");
        }
    }
}
