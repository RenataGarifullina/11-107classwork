﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    class TheatreStudent : Student, IActor
    {
        public TheatreStudent(string s) : base(s) { }

        public void Play()
        {
            Console.WriteLine("Студент актёр театра играет.");
        }

        public override void Study()
        {
            StudyTheory();
            Play();
        }

        public override void StudyTheory()
        {
            Console.WriteLine("Студент актёр таетра изучает теорию.");
        }
    }
}
