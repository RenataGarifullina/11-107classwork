﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Аспирант
    /// </summary>
    public class KpfuGraduent : KpfuStudent
    {
        /// <summary>
        /// Научный руководитель
        /// </summary>
        public KpfuWorker ScientificDirector { get;set;}

        public KpfuGraduent(string fio, DateTime birthday, KpfuWorker scientificDirector) 
            :base(fio, birthday)
        {
            ScientificDirector = scientificDirector;
            Console.WriteLine($"Отработал констурктор класса KpfuGraduent для {FIO}");
        }
    }
}
