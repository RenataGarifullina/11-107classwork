﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Человек, имеющий отношение к КПФУ
    /// </summary>
    public class KpfuPerson
    {
        /// <summary>
        /// ФИО
        /// </summary>
        public string FIO { get; set; }
        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime Birthday { get; set; }
        /// <summary>
        /// Дата начала принадлежности Университету 
        /// (для студентов дата начала учебы, для работников дата начала работы)
        /// </summary>
        public DateTime DateFrom { get; set; }
        /// <summary>
        /// Дата окончания принадлежности Университету 
        /// (для студентов дата окончания учебы, для работников дата увольнения)
        /// </summary>
        public DateTime DateTo { get; set; }

        public KpfuPerson(string fio, DateTime birthday)
        {
            FIO = fio;
            Birthday = birthday;

            Console.WriteLine($"Отработал констурктор класса KpfuPerson для {FIO}");
        }
    }
}
