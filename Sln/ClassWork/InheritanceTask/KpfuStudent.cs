﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    public class KpfuStudent : KpfuPerson
    {
        /// <summary>
        /// Номер группы
        /// </summary>
        public int GroupNumber { get; set; }
        /// <summary>
        /// Успеваемость
        /// </summary>
        public double Mark { get; set; }

        public KpfuStudent(string fio, DateTime birthday) : 
            base(fio, birthday) 
        {
            Console.WriteLine($"Отработал констурктор класса KpfuStudent для {FIO}");
        }
    }
}
