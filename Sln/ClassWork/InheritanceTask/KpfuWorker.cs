﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    public class KpfuWorker: KpfuPerson
    {
        /// <summary>
        /// Занимаемая долность
        /// </summary>
        public string Post { get; set; }
        /// <summary>
        /// Ставка
        /// </summary>
        public RationalFraction Rate { get; set; }
        /// <summary>
        /// внутренний сотрудник? да, иначе внешний
        /// </summary>
        public bool IsInternal { get; set; }

        public KpfuWorker(string fio, DateTime birthday, string post, bool isIntern)
            : base(fio, birthday)
        {
            Post = post;
            IsInternal = isIntern;
            Console.WriteLine($"Отработал констурктор класса KpfuWorker для {FIO}");
        }
    }
}
