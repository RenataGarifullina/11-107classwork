﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Вычисление синуса
    /// </summary>
    public class MacLoren
    {
        public void Run()
        {
            Console.Write("Введите x - аргумент: ");
            double x = Convert.ToDouble(Console.ReadLine()) * Math.PI / 180;
            double res_1 = Math.Sin(x);

            Console.Write("Введите точность: ");
            double accuracy = Convert.ToDouble(Console.ReadLine());

            double res_2 = 0;
            int count = 0;
            while (Math.Abs(Math.Abs(res_1) - Math.Abs(res_2)) > accuracy)
            {
                res_2 += Math.Pow(-1, count) * Math.Pow(x, 2 * count + 1) / Factorial(2 * count + 1);
                count += 1;
            }

            Console.WriteLine(res_1);
            Console.WriteLine(res_2);
        }

        static int Factorial(int n)
        {
            int res = 1;
            for (int i = n; i > 1; i--)
            {
                res *= i;
            }
            return res;

        }
    }
}

