﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    public class MethodLesson
    {
        public void Run()
        {
            Console.WriteLine("Введите число х: ");
            int number = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Число Фибоначчи под номером х = " + Fibonacci(number));
            Console.ReadLine();

            int i = ReadIntFromConsole();

            double av1;
            var res1 = Average(out av1);
            var res2 = Average(out av1, 9);
            var res3 = Average(out av1, 3, 7, 99);
            var res4 = Average(out av1,
                new int[] { 1, 2, 3 });

            var xArr = new int[] { 4, 5, 6, 9 };
            WriteArrayOnConsole(xArr);
            WriteArrayOnConsole(new int[] { 4, 5, 6, 9 });

            var x2Arr = new int[,]
                {
                    { 4, 7, 9},
                    { 55, 842, 5}
                };
            WriteArrayOnConsole(x2Arr);

            //с использованием примитивных типов
            var square = RecSquare(4, 5);
            //С использованием класса
            //r - экземпляр класса RectangleClass
            //r - переменная типа RectangleClass
            RectangleClass r = new RectangleClass();
            r.Length = 4;
            r.Width = 5;
            square = RecSquare(r);

            //с использованием структуры
            RectangleStruct rs = new RectangleStruct();
            rs.Length = 4;
            rs.Width = 5;
            square = RecSquare(rs);

            //Вызов в градусах - по умолчанию
            var sin = CustomSin(30);
            //Вызов в радианах - надо это указать явно
            var radian = 30 * Math.PI / 180;
            sin = CustomSin(radian, true);
        }

        public int Fibonacci(int number)
        {
            if (number == 0)
                return 0;
            else if (number == 1)
                return 1;
            return Fibonacci(number - 1) + Fibonacci(number - 2);
        }

        static double CustomSin(double x, bool isRadian = false)
        {
            if (!isRadian)
                x = x * Math.PI / 180;
            return Math.Sin(x);
        }

        public double RecSquare(double length,
            double width)
        {
            return length * width;
        }

        public double RecSquare(RectangleClass r)
        {
            return r.Length * r.Width;
        }

        public double RecSquare(RectangleStruct r)
        {
            return r.Length * r.Width;
        }

        public int MaxValue(params int[] inArray)
        {
            if (inArray != null || inArray.Length == 0)
            {
                Console.WriteLine("Нет данных " +
                    "для вычисления максимума");
                return Int32.MinValue;
            }
            var maxValue = inArray[0];
            for (int i = 1; i < inArray.Length; i++)
                CompareTwoValues(inArray[i],
                    ref maxValue);
            return maxValue;
        }

        public void CompareTwoValues(int currValue,
            ref int maxValue)
        {
            if (currValue > maxValue)
                maxValue = currValue;
        }

        public void WriteArrayOnConsole(int[] intArr)
        {
            //Выводим массив 
            if (intArr == null)
            {
                Console.WriteLine("Передано значение null" +
                    " в метод выведения массива");
                return;
            }

            if (intArr.Length == 0)
            {
                Console.WriteLine("Передан массив без элементов" +
                    " в метод выведения массива");
                return;
            }

            for (int i = 0; i < intArr.Length; i++)
                Console.Write($"{intArr[i]} ");
            //Console.Write(intArr[i] + " ");
            Console.WriteLine();
        }

        public void WriteArrayOnConsole(int[,] intArr)
        {
            //Выводим массив
            if (intArr == null)
            {
                Console.WriteLine("Передано значение null" +
                    " в метод выведения массива");
                return;
            }

            if (intArr.Length == 0)
            {
                Console.WriteLine("Передан массив без элементов" +
                    " в метод выведения массива");
                return;
            }

            for (int i = 0; i < intArr.GetLength(0); i++)
            {
                for (int j = 0; j < intArr.GetLength(1); j++)
                    Console.Write($"{intArr[i, j]} ");
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Считывает с консоли целое число и возвращает его
        /// </summary>
        public int ReadIntFromConsole()
        {
            int result = Int32.MinValue;
            bool goodResult = false;
            while (!goodResult)
            {
                Console.WriteLine("Введите целое число");
                var s = Console.ReadLine();

                goodResult =
                    Int32.TryParse(s, out result);
                if (!goodResult)
                    Console.WriteLine("Это не целое число." +
                        " Попробуем еще раз.");
            }
            return result;
        }

        /// <summary>
        /// Вычисление среднего 
        /// </summary>
        /// <param name="result">среднее</param>
        /// <param name="inArray">входные пареметры</param>
        /// <returns>успешно/не успешно</returns>
        public bool Average(out double result, params int[] inArray)
        {
            if (inArray.Length == 0)
            {
                Console.WriteLine(
                    "Нет входных данных для " +
                    "вычисления среднего");
                //Просто надо чем-то инициализировать result
                result = Double.NaN;
                return false;
            }
            double sum = 0;
            for (int i = 0; i < inArray.Length; i++)
            {
                sum += inArray[i];
            }
            result = sum / inArray.Length;
            return true;
        }
    }
}
