﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Родительский класс.
    /// </summary>
    public class Parent
    {
        public Parent() 
        {
            PublicField = 3;
            ProtectedField = 4;
            PrivateField = 5;
            Console.WriteLine("Сработал родительский конструктор без параметров");
        }

        public Parent(int publicFieldValue, int protFieldValue)
        {
            PublicField = publicFieldValue;
            ProtectedField = protFieldValue;
            Console.WriteLine("Сработал родительский конструктор с параметрами");
        }

        /// <summary>
        /// публичное поле
        /// </summary>
        public int PublicField;
        /// <summary>
        /// Видно только в дочернем классе
        /// </summary>
        protected int ProtectedField;
        /// <summary>
        /// Приватное поле, видно только в родительском классе
        /// </summary>
        private int PrivateField;
        /// <summary>
        /// Виртуальный метод, может быть переопределен в дочернем классе
        /// </summary>
        public virtual void VirtualMethod() 
        {
            Console.WriteLine("Виртуальный метод, работает в родительском классе");
        }

        public void UsualMethod() 
        {
            Console.WriteLine("Сработал невиртуальный метод базового класса");
        }
    }
}
