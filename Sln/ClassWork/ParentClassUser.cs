﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Учебный клас-пользователь родительского класса 
    /// </summary>
    public class ParentClassUser
    {
        /// <summary>
        /// Метод, использу.щий родительский класс
        /// </summary>
        public void UseParentClass(Parent parent)
        {
            var p = parent.PublicField * 2;
            Console.WriteLine($"Публичное поле умножаем на 2 :{p}");
            parent.VirtualMethod();
            parent.UsualMethod();
        }
    }
}
