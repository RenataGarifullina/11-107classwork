﻿using System.Diagnostics;
using System;

namespace ClassWork
{
    public class Pow
    {
        public void Run() 
        {
            var sw = new Stopwatch();
            sw.Start();
            var res1 = PowCycle(2, 20);
            sw.Stop();

            var swRec = new Stopwatch();
            swRec.Start();
            var res2 = PowRec(2, 20);
            swRec.Stop();

            Console.WriteLine($"Результат вычисления в цикле: {res1}," +
                $" время вычисления: {sw.ElapsedTicks}");
            Console.WriteLine($"Результат вычисления в рекурсии: {res2}," +
                $" время вычисления: {swRec.ElapsedTicks}");
        }

        /// <summary>
        /// Вычисление степени в цикле
        /// </summary>
        public int PowCycle(int x, int n) 
        {
            int result = 1;
            while (n > 0)
            {
                result *= x;
                n--;
            }
            return result;
        }

        public int PowRec(int x, int n) 
        {
            if (x == 0)
                return 0;
            if (n == 0)
                return 1;
            if (n == 1)
                return x;
            //нечетная степень
            if (n % 2 == 1)
                return x * PowRec(x, n - 1);
            //четная степень
            else 
            {
                int z = PowRec(x, n / 2);
                return z * z;
            }
        }
    }
}
