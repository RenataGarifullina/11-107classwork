﻿using System;
using ClassWork.Extension;

namespace ClassWork
{
    class Program
    {
        static void Main(string[] args)
        {

            #region старые примеры


            //var animal = new Aquatic(-273, false, "cat", "Boris", 1, FeedType.Omnivore, 25, true, Sex.Female);
            //Console.WriteLine(animal.ToString());
            //object o;


            //string s = "Методы расширения позволяют \"добавлять\" методы в существующие типы без создания нового производного типа, перекомпиляции и иного изменения первоначального типа. Методы расширения представляют собой разновидность статического метода, но вызываются так же, как методы экземпляра в расширенном типе.";
            ////s.Rainbow();
            //var rf = new RationalFraction(1, 2);
            ////Console.WriteLine(rf.IsInteger());

            //var singer = new SingerStudent("singer");
            ////singer.Sing();

            //var painter = new PainterStudent("painter");
            //((IPainter)painter).ApplyPaint();

            //var rcv1 = new RationalComplexVector2D
            //    (new RationalComplexNumber(
            //        new RationalFraction(1, 2),
            //        new RationalFraction(3, 4)),
            //     new RationalComplexNumber(
            //         new RationalFraction(5, 6),
            //         new RationalFraction(7, 8)));

            //var rcv2 = rcv1.Clone();  // rcv2 - тип object

            ////явно привели к дочернему типу
            //var rvc3 = (RationalComplexVector2D)rcv2;

            //rcv1.AddToYourself(new RationalComplexVector2D
            //    (new RationalComplexNumber(
            //        new RationalFraction(1, 1),
            //        new RationalFraction(1, 1)),
            //     new RationalComplexNumber(
            //         new RationalFraction(1, 1),
            //         new RationalFraction(1, 1))));


            //rvc3.AddToYourself(new RationalComplexVector2D
            //    (new RationalComplexNumber(
            //        new RationalFraction(2, 1),
            //        new RationalFraction(2, 1)),
            //     new RationalComplexNumber(
            //         new RationalFraction(2, 1),
            //         new RationalFraction(2, 1))));


            ////Child1 child1 = new Child1();
            ////Parent parent1 = child1;
            ////Parent parent2 = new Parent();
            ////child1.VirtualMethod();
            ////parent1.VirtualMethod();//должен отработать дочерний
            ////child1.UsualMethod();
            ////parent1.UsualMethod();

            ////var parent3 = new Parent();
            ////var child2 = new Child2();
            ////parent3 = child2;
            ////child1 = (Child1)parent1;
            //////child1 = (Child1)parent3; - InvalidCastException ошибка приведения типов
            ////var child3 = parent3 as Child1;  //доложен вернуться null
            ////child1 = parent1 as Child1;  //все хорошо

            ////if (parent3 is Child1)
            ////{
            ////    Console.WriteLine("Приведение типов возоможно");
            ////}

            //////вариант 1
            ////Child1 child4;
            ////if (parent1 is Child1)
            ////    child4 = parent1 as Child1;
            //////вариант 2
            ////if (parent1 is Child1 child5)
            ////    child4 = child5;


            //////пример полиморфизма
            ////var pu = new ParentClassUser();
            ////pu.UseParentClass(parent2);  //экземпляр типа Parent
            ////pu.UseParentClass(child1);  //экземпляр типа Child1
            ////pu.UseParentClass(child2);   //экземпляр типа Child2

            ////DogIll dogill = new DogIll()
            ////{
            ////    Name = "Больной питомец",
            ////    Age = 10,
            ////    Diagnosis = "Больная нога"
            ////};
            ////Dog dog = new Dog()
            ////{
            ////    VeterinarianVisit = "Да",
            ////    PassportYesOrNot = true
            ////};
            ////dog = dogill;
            ////dog.Taking_Care();
            ////dog.Feed();



            ////var food = new Food()
            ////{
            ////    Name = "Pizza",
            ////    Weight = 12,
            ////    Time = 1
            ////};
            ////var company = new Company();
            ////company.Delivery(food);
            #endregion

            var c1 = new RefTypeExampleClass() { IntField = 5 };
            var s1 = new ValueTypeExampleStruct() { IntField = 5 };

            var c2 = c1;
            var s2 = s1;

            c1.IntField = 7;
            s1.IntField = 7;

        }

        public class RefTypeExampleClass 
        {
            public int IntField;
        }

        public struct ValueTypeExampleStruct
        {
            public int IntField;
        }

        /// <summary>
        /// Вывести информацию о родителях
        /// </summary>
        /// <param name="person">Человек, родителей которого выводит метод</param>
        static void WriteParens(Person1 person)
        {
            if (person == null)
            {
                Console.WriteLine("Человек не задан");
                return;
            }

            //1 вариант
            var result = string.Empty;   //var result = "";
            if (person.Mother != null)
                result += $" Мама {person.Mother.Surname ?? ""} {person.Mother.Name ?? string.Empty} {person.Mother.Patronymic ?? ""}";
            //  ?? если переменная равна null, то вместо нее подставится значение после ?? 
            // ?? аналог кода: if (person.Mother.Patronymic == null) "" else person.Mother.Patronymic;

            result += person.Father != null
                ? $" Папа {person.Father.Surname ?? ""} {person.Father.Name ?? string.Empty} {person.Father.Patronymic ?? ""}"
                : string.Empty;


            //2 вариант
            var result2 = (person.Mother != null
                ? $" Мама {person.Mother.Surname ?? ""} {person.Mother.Name ?? string.Empty} {person.Mother.Patronymic ?? ""}"
                : string.Empty)
                +
                (person.Father != null
                ? $" Папа {person.Father.Surname ?? ""} {person.Father.Name ?? string.Empty} {person.Father.Patronymic ?? ""}"
                : string.Empty);

            Console.WriteLine(result2);
        }
    }

    /// <summary>
    /// Самый простой класс
    /// </summary>
    public class Simple
    { 
    }

    /// <summary>
    /// Человек
    /// </summary>
    public class Person1
    {
        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname;

        /// <summary>
        /// Имя
        /// </summary>
        public string Name;

        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronymic;

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime Birthday;

        /// <summary>
        /// Регион рождения
        /// </summary>
        public int RegionOfBirthday;

        /// <summary>
        /// Мама
        /// </summary>
        public Person1 Mother;

        /// <summary>
        /// Папа
        /// </summary>
        public Person1 Father;
    }

    /// <summary>
    /// Класс Математический для тестирования константы
    /// </summary>
    public class CustomMath 
    {
        public const double Pi = 3.14159;
    }

    /// <summary>
    /// Класс Человек с методами
    /// </summary>
    public class Person2
    {
        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname;

        /// <summary>
        /// Имя
        /// </summary>
        public string Name;

        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronymic;

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime Birthday;

        /// <summary>
        /// Регион рождения
        /// </summary>
        public int RegionOfBirthday;

        /// <summary>
        /// Мама
        /// </summary>
        public Person2 Mother;

        /// <summary>
        /// Папа
        /// </summary>
        public Person2 Father;

        /// <summary>
        /// Возраст
        /// </summary>
        /// <returns>Возраст в годах</returns>
        //public int Age() 
        //{
        //    return DateTime.Now.Year - Birthday.Year;
        //}
        public int Age() => DateTime.Now.Year - Birthday.Year;


        /// <summary>
        /// Задаем родителей
        /// </summary>
        /// <param name="Mother">Мать</param>
        /// <param name="Father">Отец</param>
        public void SetParents(Person2 mother, Person2 father) 
        {
            Mother = mother;
            Father = father;
        }

        /// <summary>
        /// Задаем родителей
        /// </summary>
        public void SetParents(Person2 mother, Person2 father, Person2 grandMaMa)
        {
            Mother = mother;
            Father = father;
            if (mother != null)
                mother.Mother = grandMaMa;
        }
    }

    /// <summary>
    /// Класс Человек со свойствами и конструктором
    /// </summary>
    public class Person3
    {
        private string name;
        private string surname;
        private DateTime birthday;
        public string Name 
        {
            get { return name; }
            set 
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    Console.WriteLine("Пришло пустое имя, данные не записаны");
                    return;
                }
                if (value.Length > 10)
                    Console.WriteLine("Имя больше 10 символов, будет обрезано");
                name = value.Length > 10 ? value.Substring(0, 10) : value; 
            }
        }


        public string Surname 
        { 
            get { return surname; }
            set 
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    Console.WriteLine("Пришло пустое фамилия, данные не записаны");
                    return;
                }
                surname = value;
            }
        }

        public DateTime Birthday {
            get { return birthday; }
            set 
            {
                if ( DateTime.Now.Year - value.Year > 1000)
                {
                    Console.WriteLine($"Слишком большой возраст {DateTime.Now.Year - value.Year}, данные не сохранены");
                    return;
                }
                birthday = value;
            }
        }

        /// <summary>
        /// Отчество - автоматически реализуемое свойство
        /// </summary>
        public string Patronymic { get; set; }


        public int Age 
        {
            get 
            { 
                return DateTime.Now.Year - Birthday.Year; 
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public Person3(string surname, string name, DateTime birthday) 
        {
            Surname = surname;
            Name = name;
            Birthday = birthday;
        }
    }
}
