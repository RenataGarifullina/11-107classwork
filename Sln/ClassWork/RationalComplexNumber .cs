﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// комплексное число, компонентами которого являются рациональные дроби
    /// </summary>
    public class RationalComplexNumber
    {
        /// <summary>
        /// действительная часть
        /// </summary>
        private RationalFraction real;
        /// <summary>
        /// мнимая часть
        /// </summary>
        private RationalFraction image;
        /// <summary>
        /// Действительная часть
        /// </summary>
        public RationalFraction Real 
        {
            get { return real; }
        }
        /// <summary>
        /// мнимая часть
        /// </summary>
        public RationalFraction Image
        {
            get { return image; }
        }
        /// <summary>
        /// конструктор без параметров
        /// </summary>
        public RationalComplexNumber() 
        {
            image = new RationalFraction();
            real = new RationalFraction();
        }
        /// <summary>
        /// Конструктор с парамтерами
        /// </summary>
        public RationalComplexNumber(RationalFraction r, RationalFraction i)
        {
            real = r;
            image = i;
        }
        /// <summary>
        /// Сложение двух комплексных чисел
        /// </summary>
        public RationalComplexNumber Add(RationalComplexNumber n) 
        {
            return new RationalComplexNumber(real.Add(n.Real), image.Add(n.Image));
        }
        /// <summary>
        /// Вычитание
        /// </summary>
        public RationalComplexNumber Sub(RationalComplexNumber n) 
        {
            return new RationalComplexNumber(real.Sub(n.real), image.Sub(n.image));
        }
        /// <summary>
        /// Перемножение дробей
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public RationalComplexNumber Mult(RationalComplexNumber n)
        {
            return new RationalComplexNumber(
                real.Mult(n.real).Add(image.Mult(n.image)),
                real.Mult(n.image).Add(image.Mult(n.real)));
        }
        public override string ToString() 
        {
            return $"{real.RFToString()} + i*{image.RFToString()}";
        }
    }
}
