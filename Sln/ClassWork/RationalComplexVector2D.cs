﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime;

namespace ClassWork
{
    /// <summary>
    /// двумерный вектор, компоненты которого являются комплексными числами, 
    /// компонентами которого являются рациональные дроби
    /// </summary>
    /// 

    //public interface ICloneable 
    //{
    //    object Clone();
    //}

    public class RationalComplexVector2D : ICloneable
    {
        /// <summary>
        /// координата х
        /// </summary>
        private RationalComplexNumber x;
        /// <summary>
        /// координата y
        /// </summary>
        private RationalComplexNumber y;

        /// <summary>
        /// координата х
        /// </summary>
        public RationalComplexNumber X
        {
            get { return x; }
        }

        /// <summary>
        /// координата y
        /// </summary>
        public RationalComplexNumber Y
        {
            get { return y; }
        }
        /// <summary>
        /// Конструктор без параметров
        /// </summary>
        public RationalComplexVector2D() 
        {
            x = new RationalComplexNumber();
            y = new RationalComplexNumber();
        }
        /// <summary>
        /// Конструктор с параметрами
        /// </summary>
        public RationalComplexVector2D(RationalComplexNumber x, RationalComplexNumber y)
        {
            //имена входящих параметров и внутренних приватных переменных специально 
            //совпадают, чтобы отработать клучевое слово this
            this.x = x;
            this.y = y;
        }
        /// <summary>
        /// Сложение двух векторов
        /// </summary>
        public RationalComplexVector2D Add(RationalComplexVector2D n)
        {
            return new RationalComplexVector2D(x.Add(n.X), y.Add(n.y));
        }

        public void AddToYourself(RationalComplexVector2D n) 
        {
            x = x.Add(n.X);
            y = y.Add(n.y); 
        }
        public override string ToString()
        {
            return $"({x.ToString()},{y.ToString()})";
        }
        /// <summary>
        /// Скалярное произведение
        /// </summary>
        public RationalComplexNumber ScalarProduct() 
        {
            throw new NotImplementedException();
        }

        public object Clone()
        {
            var newX = CloneOneCoor(x);
            var newY = CloneOneCoor(y);
            return new RationalComplexVector2D(newX, newY);
        }

        private RationalComplexNumber CloneOneCoor(RationalComplexNumber coord)
        { 
            //новая действительная часть
            var real = new RationalFraction(coord.Real.A, coord.Real.B);
            //новая мнимая часть
            var image = new RationalFraction(coord.Image.A, coord.Image.B);
            var newX = new RationalComplexNumber(real, image);
            return newX;
        }

    }
}
