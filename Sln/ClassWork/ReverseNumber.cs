﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    public class ReverseNumber
    {
        public void Run()
        {
            int num = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(Reverse(num, 0));
        }

        public int Reverse(int x, int y)
        {
            if (x == 0)
                return y;

            else
               return Reverse(x / 10, y * 10 + x % 10);
        }



    }
}
