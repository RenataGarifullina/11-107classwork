﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// sealed - запрещает наследоваться от данного класса
    /// </summary>
    public sealed class SealedClass
    {
    }
}
