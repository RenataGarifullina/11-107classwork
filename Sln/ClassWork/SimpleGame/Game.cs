﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork.SimpleGame
{
    /// <summary>
    /// Игра
    /// </summary>
    public class Game
    {
        public Player Player1;
        public Player Player2;
        private const int startHealth = 9;
        public Game(string player1_name, string player2_name)
        {
            Player1 = new Player(player1_name);
            Player2 = new Player(player2_name);
        }
        public void Play()
        {
            Player1.HealthPoint = startHealth;
            Player2.HealthPoint = startHealth;
            Random random = new Random();
            int r1 = random.Next(0, 2);
            while (Player1.HealthPoint > 0 && Player2.HealthPoint > 0)
            {
                int power = random.Next(1, 10);
                if (r1 == 0)
                {
                    Player2.HealthPoint -= power;
                    r1 = 1;
                    Console.WriteLine($"{Player1.Nickname} нанёс удар с силой {power}");
                }
                else
                {
                    Player1.HealthPoint -= power;
                    r1 = 0;
                    Console.WriteLine($"{Player2.Nickname} нанёс удар с силой {power}");
                }
            }
            if (Player1.HealthPoint > 0)
                Console.WriteLine($"{Player1.Nickname} одержал победу");
            else
                Console.WriteLine($"{Player2.Nickname} одержал победу");
        }
    }
}
