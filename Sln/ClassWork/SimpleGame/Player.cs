﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork.SimpleGame
{
    public class Player
    {
        /// <summary>
        /// Здоровье
        /// </summary>
        public int HealthPoint { get; set; }
        public string Nickname { get; set; }
        public Player(string name) 
        {
            Nickname = name;
        }
    }
}
