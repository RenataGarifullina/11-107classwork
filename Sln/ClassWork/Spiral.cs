﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Вывести все элементы массива по спирали
    /// </summary>
    public class Spiral
    {
        public void Run() 
        {
            //var arr = new int[3, 4]
            //{
            //    { 1, 2, 3, 4 },
            //    { 5, 6, 7, 8 },
            //    { 9, 10, 11, 12 }
            //};

            //var arr = new int[1,1]
            //{
            //    { 1 }
            //};

            var arr = new int[4, 4]
             {
                { 1, 2, 3, 4 },
                { 5, 6, 7, 8 },
                { 9, 10, 11, 12 },
                { 13, 14, 15, 16}
             };

            //отступ
            var indent = 0;
            var row = arr.GetLength(0);
            var col = arr.GetLength(1);
            var stepCount = row / 2 + row % 2;
            for (int i = 1; i <= stepCount; i++)
            {
                //выводим верхнюю строку
                for (var j = indent; j < col - indent; j++)
                    Console.Write(arr[indent, j] + " ");

                //выводим правый край
                if (row > indent + indent + 2)
                {
                    for (var j = indent + 1; j < row - indent - 1; j++)
                        Console.Write(arr[j, col - 1 - indent] + " ");
                }
                //выводим нижнюю строку
                if (row > indent + indent + 1)
                {
                    for (var j = col - indent - 1; j >= indent; j--)
                        Console.Write(arr[row - 1 - indent, j] + " ");
                }

                //выводим левый край
                if (row > indent + indent + 2)
                {
                    for (var j = row - indent - 2; j > indent; j--)
                        Console.Write(arr[j, indent] + " ");
                }
                indent++;
            }
        }
    }
}
