﻿using System;

namespace ClassWork
{
    /// <summary>
    /// Класс со статическим конструктором
    /// </summary>
    public class ClassWithStaticConstructor
    {
        /// <summary>
        /// Информационное поле
        /// </summary>
        public int InfoField { get; set; }

        /// <summary>
        /// Поле, считающее количество созданных экземпляров
        /// </summary>
        private static int objectCounter = 0;

        /// <summary>
        /// Свойство с количеством созданных экземпляров
        /// </summary>
        public static int ObjectCounter { get { return objectCounter; } }

        /// <summary>
        /// Статический конструктор
        /// </summary>
        static ClassWithStaticConstructor() 
        {
            Console.WriteLine("Сработал статический конструктор");
        }

        public ClassWithStaticConstructor(int value)
        {
            Console.WriteLine("Сработал обычный конструктор без параметров");
            objectCounter++;
            InfoField = value;
        }
        /// <summary>
        /// Просто статический метод для примера
        /// </summary>
        public static void WriteToConsole() 
        {
            Console.WriteLine("Я консольный метод, я работаю");
        }

    }
}
