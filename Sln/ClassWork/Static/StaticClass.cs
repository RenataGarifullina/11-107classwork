﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    public static class StaticClass
    {
        static StaticClass() 
        {
            stringValue = "FromStaticConst";
            Value = Int32.MaxValue; 
            Console.WriteLine("Статический конструктор класса StaticClass сработал");
        }

        public static int Value;

        private static string stringValue;
        public static string StringValue { 
            get 
            { 
                Console.WriteLine("Получение значения статического свойства"); 
                return stringValue; 
            }
            set { stringValue = value; }
        }

        public static int DoubleValue() 
        {
            Console.WriteLine("Статический метод в действии");
            return Value * 2;
        }
    }
}
