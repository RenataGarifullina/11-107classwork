﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Зубная щетка
    /// </summary>
    class ToothBrush
    {
        /// <summary>
        /// Сколько щеток в стаканчике
        /// </summary>
        static int ToothBrushCup;
        /// <summary>
        /// Статический конструктор
        /// </summary>
        static ToothBrush() 
        {
            ToothBrushCup = 0;
        }

        public static void WriteBrushCount() 
        {
            Console.WriteLine($"В стаканчике {ToothBrushCup} щеток");
        }

        private bool inCup;
        public string Owner { get; }
        public string Colour { get; }
        public ToothBrush(string owner, string colour) 
        {
            Owner = owner;
            Colour = colour;
        }
        /// <summary>
        /// Положить в стакан
        /// </summary>
        public void PutInCup() 
        {
            if (inCup)
                Console.WriteLine("Щетка уже в чашке");
            else
            {
                inCup = true;
                ToothBrushCup++;
                Console.WriteLine($"В стаканчик положили {Colour} щетку {Owner} ");
            }
        }
        /// <summary>
        /// Вынуть из стакана
        /// </summary>
        public void GetFromCup()
        {
            if (inCup)
            {
                inCup = false;
                ToothBrushCup--;
                Console.WriteLine($"Из стаканчика вынули {Colour} щетку {Owner} ");
            }
            else
                Console.WriteLine($"{Colour} щетки владельца {Owner} в стакане нет");
        }
    }
    
}
