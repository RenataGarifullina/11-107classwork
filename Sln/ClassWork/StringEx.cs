﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    public class StringEx
    {
        public void Run()
        {
            int result = StringOrder("Mother", "Mother");
            int result1 = StringOrder("Father", "Mother");
            int result2 = StringOrder("Mother", "Father");

            string[] sArray = 
                new string[] { "hi", "my", "name", "is", "Renata" };

            var resArray = BubbleSort(sArray);

        }

        /// <summary>
        /// 0 - правильный порядок, 1 - обратный порядок, 2 - одинаковые слова
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public int StringOrder(string s1, string s2)
        {
            int i = 0;
            while(i<s1.Length && i<s2.Length)
            {
                if (s1[i] < s2[i])
                    return 0;
                if (s1[i] > s2[i])
                    return 1;
                i++;
            }

            if (i < s2.Length) return 0;
            if (i < s1.Length) return 1;
            return 2;

        }

        public string[] BubbleSort(string[] arr) 
        {
            //Больше нет неупорядоченных элементов
            bool isSorted = true;

            do
            {
                isSorted = true;
                for (int i = 0; i < arr.Length - 1; i++)
                    if (StringOrder(arr[i], arr[i + 1]) == 1) 
                    {
                        isSorted = false;
                        string s = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = s;
                    }
            }
            while (!isSorted);

            return arr;
        }
    }
}
