﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Вывести все элементы массива в одну строку для нечетных строку слева направо, для четных строк справа налево
    /// </summary>
    public class TreeBEx
    {
        public void Run() 
        {
            //считываем количество строк и столбцов
            var col = Int32.Parse(Console.ReadLine());
            var row = Int32.Parse(Console.ReadLine());

            //создаем массив
            int[,] array = new int[row, col];

            //читаем с экрана и заполняем
            for (int i = 0; i < row; i++)
                for (int j = 0; j < col; j++)
                    array[i, j] = Int32.Parse(Console.ReadLine());


            //выводим на экран исходный массив
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                    Console.Write(array[i, j] + " ");
                Console.WriteLine();
            }

            //чтение туда-сюда и вывод в одну строку
            for (int i = 0; i < row; i++)
            {
                if (i % 2 == 0)
                {
                    for (int j = 0; j < col; j++)
                        Console.Write(array[i, j] + " ");
                }
                else
                {
                    for (int j = col - 1; j >= 0; j--)
                        Console.Write(array[i, j] + " ");
                }
            }

            Console.ReadKey();
        }
    }
}
