﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    public class Vector
    {
        public double X { get; set; }
        public double Y { get; set; }
        public Vector() { X = 0; Y = 0; }
        public Vector(double x_coordinate, double y_coordinate)
        {
            X = x_coordinate;
            Y = y_coordinate;
        }
        public Vector Add(Vector vector)
        {
            return new Vector(X + vector.X, Y + vector.Y);
        }
        public void Add_to_Yourself(Vector vector)
        {
            X += vector.X;
            Y += vector.Y;
        }
        public Vector Sub(Vector vector)
        {
            return new Vector(X - vector.X, Y - vector.Y);
        }
        /// <summary>
        /// Просто для примера: статический метод, вычисляющий сумму векторов и возвращающий результат как новый вектор
        /// </summary>
        public static Vector Add(Vector vector1, Vector vector2)
        {
            return new Vector(vector1.X + vector2.X, vector1.Y + vector2.Y);
        }
        public void Sub_to_Yourself(Vector vector)
        {
            X -= vector.X;
            Y -= vector.Y;
        }
        public Vector Mult(double coefficient)
        {
            return new Vector(X * coefficient, Y * coefficient);
        }
        public void Mult_to_Yourself(double coefficient)
        {
            X *= coefficient;
            Y *= coefficient;
        }
        public void Vector_ToString()
        {
            Console.WriteLine($"Vector({X};{Y})");
        }
        public double Vector_Length()
        {
            double length = Math.Sqrt(X * X + Y * Y);
            return length;
        }
        public double Vector_Scalar_Mult(Vector vector)
        {
            double scalar_mult = vector.X * X + vector.Y * Y;
            return scalar_mult;
        }
        public double Cosinus(Vector vector)
        {
            double cosinus = Vector_Scalar_Mult(vector) / (Vector_Length() * vector.Vector_Length());
            return cosinus;
        }
        public bool Equality_of_Vectors(Vector vector)
        {
            return X == vector.X && Y == vector.Y;
        }
    }
}
