﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork
{
    /// <summary>
    /// Класс инструментов для работы с векторами
    /// </summary>
    static class VectorUtility
    {
        /// <summary>
        /// Складывает два вектора и результат возвращает новым вектором
        /// </summary>
        public static Vector Add(Vector vector1, Vector vector2)
        {
            return new Vector(vector1.X + vector2.X, vector1.Y + vector2.Y); 
        }
        /// <summary>
        /// Вычисляет разницу векторов и возвращает результат как новый вектор
        /// </summary>
        public static Vector Sub(Vector v1, Vector v2)
        {
            return new Vector(v1.X - v2.X, v1.Y - v2.Y);
        }
        public static Vector Mult(Vector v, int coef)
        {
            return new Vector(v.X * coef, v.Y * coef);
        }
    }
}
