using System;

class TestCscClass
{
	//количество неотрицательных чисел из трех
	static void Main()
	{
		string xString = Console.ReadLine();
		int x = Int32.Parse(xString);
		
		string yString = Console.ReadLine();
		int y = Convert.ToInt32(yString);
		
		string zString = Console.ReadLine();
		int z;
		if(!Int32.TryParse(zString, out z))
			return;
						
		int count = 0;
		
		if (x >= 0) 
		{
			count = count + 1;
		}
		
		if (y >= 0) 
		{
			count = count + 1;
		}
		
		if (z >= 0) 
		{
			count = count + 1;
		}
		
		Console.WriteLine(count);
		Console.ReadLine();
	}
}