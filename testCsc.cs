using System;

class TestCscClass
{
	//вычисление арифметического выражения
	static void Main()
	{
		int a = 5;
		int b = 10;
		
		double y;
		double res_1 = a - 2.3*b*b;
		double res_2 = (b - 1.2) / b;
		
		/*
		double maximum;
		
		if (res_1 >= res_2)
		{
			maximum = res_1;
		}
		else
		{
			maximum = res_2;
		}
		*/
		
		double maximum = res_1 >= res_2 ? res_1 : res_2;
		
		y = a*a + maximum;
		
		Console.WriteLine(y);
		Console.ReadLine();
	}
}