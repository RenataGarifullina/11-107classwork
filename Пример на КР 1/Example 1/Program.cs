﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_1
{
    /// <summary>
    /// Пример задания на 1 контрольную работу
    /// </summary>
    class Program
    {
        /*Задание: обойти массив по спирали. 
         * Для каждого элемента, кратного трем, вычислить факториал. 
         * Посчитать количество элементов, равных семи, и вывести на консоль. 
         * Вычисление факториала и вывод на экран оформить в отдельные методы.
         * П.С. по умолчанию массив вбиваем в код без считывания. 
         * Но возможно попрошу как-нибудь считать с консоли 
         * (поскольку тут сильно много вариантов не придумаешь, 
         *      это можно посмотреть в аудиторных упражнениях)
         */
        static void Main(string[] args)
        {
            var arr = new int[4, 4]
             {
                { 1, 2, 3, 4 },
                { 5, 6, 7, 8 },
                { 9, 10, 11, 12 },
                { 13, 14, 15, 16}
             };

            //отступ
            var indent = 0;
            var row = arr.GetLength(0);
            var col = arr.GetLength(1);
            var stepCount = row / 2 + row % 2;
            int sevenCount = 0;

            #region Пробегаю по массиву

            for (int i = 1; i <= stepCount; i++)
            {
                //выводим верхнюю строку
                for (var j = indent; j < col - indent; j++)
                    ProcessItem(arr[indent, j], ref sevenCount);

                //выводим правый край
                if (row > indent + indent + 2)
                {
                    for (var j = indent + 1; j < row - indent - 1; j++)
                        ProcessItem(arr[j, col - 1 - indent], ref sevenCount);
                }
                //выводим нижнюю строку
                if (row > indent + indent + 1)
                {
                    for (var j = col - indent - 1; j >= indent; j--)
                        ProcessItem(arr[row - 1 - indent, j], ref sevenCount);
                }

                //выводим левый край
                if (row > indent + indent + 2)
                {
                    for (var j = row - indent - 2; j > indent; j--)
                        ProcessItem(arr[j, indent], ref sevenCount);
                }
                indent++;
            }
            #endregion

            WriteConsoleSevenCount(sevenCount);
        }

        static void ProcessItem(int x, ref int sevenCount) 
        {
            Console.Write(x + " ");
            if (x % 3 == 0)
                Console.Write($"(кратно трем, факториал равен {Factorial(x)}) ");
            if (x == 7)
                sevenCount++;
        }

        /// <summary>
        /// Функция, вычисляющая факториал
        /// </summary>
        /// <param name="x">x!</param>
        /// <returns></returns>
        static int Factorial(int x) 
        {
            int result = 1;
            for (int i = 2; i <= x; i++)
                result *= i;
            return result;
        }

        /// <summary>
        /// Больше как упражнение, великого смысла в этом методе нет
        /// </summary>
        /// <param name="count"></param>
        static void WriteConsoleSevenCount(int count) 
        {
            //Просто отделила строкой 
            Console.WriteLine();
            if (count > 0)
                Console.WriteLine($"Количество элементов, равных семи: {count}");
            else
                Console.WriteLine("Элементов, равных семи, нет");
        }
    }
}
